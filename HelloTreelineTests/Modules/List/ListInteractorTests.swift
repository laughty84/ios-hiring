//
//  ListInteractorTests.swift
//  HelloTreelineTests
//
//  Created by Piotr Rola on 21/03/2022.
//

import XCTest
@testable import HelloTreeline

class ListInteractorTests: XCTestCase {

    private var interactor: ListInteractor!
    private var interactorDelegateMock: InteractorDelegateMock!

    private let inventoryItems = [
        InventoryItem(id: "test-id-1", title: "title-1", description: "description-1", color: "color-1", available: 1001, cost: 9.91),
        InventoryItem(id: "test-id-2", title: "title-2", description: "description-2", color: "color-2", available: 1002, cost: 9.92),
        InventoryItem(id: "test-id-3", title: "title-3", description: "description-3", color: "color-3", available: 1003, cost: 9.93)
    ]

    override func setUpWithError() throws {
        self.interactorDelegateMock = InteractorDelegateMock()
    }

    override func tearDownWithError() throws {
        self.interactorDelegateMock = InteractorDelegateMock()
    }

    func testSuccessRequest() throws {
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = inventoryItems
        interactor = ListInteractor(itemRepository: itemRepository)
        interactor.delegate = interactorDelegateMock
        interactor.loadItems()
        XCTAssert(interactorDelegateMock.inventoryItems.count == 3)

    }

    func testFailedRequest() throws {
        let itemRepository = MockInventoryItemRepository()
        itemRepository.responseError = true
        interactor = ListInteractor(itemRepository: itemRepository)
        interactor.delegate = interactorDelegateMock
        interactor.loadItems()
        XCTAssert(interactorDelegateMock.error != nil)
    }

    func testUpdateItems() throws {
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = inventoryItems
        interactor = ListInteractor(itemRepository: itemRepository)
        interactor.delegate = interactorDelegateMock
        interactor.loadItems()
        interactor.addToItem("test-id-1")
        interactor.addToItem("test-id-1")
        interactor.removeFromItem("test-id-1")
        XCTAssert(interactorDelegateMock.inventoryItems[0].requestedAmount == 1)
    }

    func testSendingStatus() throws {
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = inventoryItems
        interactor = ListInteractor(itemRepository: itemRepository)
        interactor.delegate = interactorDelegateMock
        let exp = expectation(description: "Status true")
        interactorDelegateMock.sendingStatusCallback = { exp.fulfill() }
        interactor.sendReport()
        waitForExpectations(timeout: 5)
    }

}

fileprivate class InteractorDelegateMock: ListInteractorDelegate {
    var inventoryItems: [InventoryItem] = []
    var error: Error?
    var sendingStatusCallback: (()->Void)?

    func itemsDidLoad(items: [InventoryItem]) {
        self.inventoryItems = items
    }

    func itemsLoadFailed(error: Error) {
        self.error = error
    }

    func updateSendingStatus(_ status: SendingStatus) {
        sendingStatusCallback?()
    }

    func updateItem(_ item: InventoryItem) {
        inventoryItems[0] = item
    }


}
