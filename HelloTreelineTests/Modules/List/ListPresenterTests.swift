//
//  ListPresenterTests.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import XCTest
@testable import HelloTreeline

class ListPresenterTests: XCTestCase {
    
    private var presenter: ListPresenter!
    private var mockRouter: MockRouter!
    private var view: MockViewController!
    
    private let inventoryItems = [
        InventoryItem(id: "test-id-1", title: "title-1", description: "description-1", color: "color-1", available: 1001, cost: 9.91),
        InventoryItem(id: "test-id-2", title: "title-2", description: "description-2", color: "color-2", available: 1002, cost: 9.92),
        InventoryItem(id: "test-id-3", title: "title-3", description: "description-3", color: "color-3", available: 1003, cost: 9.93)
    ]

    override func setUpWithError() throws {
        
        self.mockRouter = MockRouter()
        self.view = MockViewController()
        
        
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = inventoryItems
        let interactor = ListInteractor(itemRepository: itemRepository)
        
        self.presenter = ListPresenter(view: view, interactor: interactor, router: mockRouter)
    }
    
    func testViewWillAppearToLoadListItems() {
        
        // before
        XCTAssertEqual(presenter.itemCount, 0)
        XCTAssertEqual(view.reloadListCalledCount, 0)
        
        // when
        presenter.viewWillAppear()
        
        // then
        XCTAssertEqual(presenter.itemCount, inventoryItems.count)
        XCTAssertEqual(view.reloadListCalledCount, 1)
        for i in 0..<(inventoryItems.count) {
            let listItem = presenter.listItem(at: IndexPath(row: i, section: 0))
            let inventoryItem = inventoryItems[i]
            XCTAssertEqual(listItem.id, inventoryItem.id)
            XCTAssertEqual(listItem.title, inventoryItem.title)
        }
    }
    
    func testListItemSelection() {
        
        // before
        XCTAssertNil(mockRouter.routeToDetailsCallObjectId)
        
        // when
        presenter.viewWillAppear()
        
        let objectId = inventoryItems[0].id
        presenter.listItemSelected(objectId: objectId)
        
        // then
        XCTAssertEqual(mockRouter.routeToDetailsCallObjectId, objectId)
    }
}

fileprivate class MockViewController: ListViewController {
    func displayError(_ message: String) {
    }

    func updateCell(row: Int) {
    }

    func displaySendStatus(_ status: String) {
    }

    
    var reloadListCalledCount = 0
    func reloadList() {
        reloadListCalledCount += 1
    }
}

fileprivate class MockRouter: ListRouter {
    
    var routeToDetailsCallObjectId: String? = nil
    func routeToDetails(objectId: String) {
        routeToDetailsCallObjectId = objectId
    }
}
