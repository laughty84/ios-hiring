//
//  MockInventoryItemRepository.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
@testable import HelloTreeline

class MockInventoryItemRepository: InventoryItemRepository {

    var responseError = false
    
    var items: [InventoryItem] = []
    
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void) {
        if responseError {
            completion(.failure(APIClientError.unknown(internalError: nil)))
        } else {
            completion(.success(items))
        }
    }
    
    func get(objectId: String) -> InventoryItem? {
        items.first(where: { $0.id == objectId })
    }
}
