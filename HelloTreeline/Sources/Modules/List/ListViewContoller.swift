//
//  ListViewContoller.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

protocol ListViewController: AnyObject {
    func reloadList()
    func displayError(_ message: String)
    func updateCell(row: Int)
    func displaySendStatus(_ status: String)
}

class ListDefaultViewController: UIViewController, ListViewController {

    static func build() -> ListDefaultViewController {
        let viewController = UIStoryboard.main.instantiateViewController(of: ListDefaultViewController.self)!
        let router = ListDefaultRouter(viewController: viewController)
        let interactor = ListInteractor()

        viewController.presenter = ListPresenter(view: viewController, interactor: interactor, router: router)

        return viewController
    }

    private var presenter: ListPresenter!

    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupSendButton()
        tableView.register(ItemCell.self, forCellReuseIdentifier: ItemCell.cellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear()
    }

    @objc
    func sendSalesReport() {
        activityIndicator.startAnimating()
        presenter.sendReport()
    }
    
    func reloadList() {
        tableView.reloadData()
    }

    func displayError(_ message: String) {
        let alert = UIAlertController(title: "Error Occured", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Refresh", style: .default, handler: {[weak self] _ in
            self?.presenter.viewWillAppear()
        }))
        present(alert, animated: true, completion: nil)
    }

    func updateCell(row: Int) {
        let indexPath = IndexPath(row: row, section: 0)

        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .none)
        tableView.endUpdates()

    }

    func displaySendStatus(_ status: String) {
        activityIndicator.stopAnimating()
        let alert = UIAlertController(title: "Sending Status", message: status, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    private func setupSendButton() {
        navigationBar.rightBarButtonItem = UIBarButtonItem(title: "Send", style: .plain, target: self, action: #selector(sendSalesReport))
    }
}

extension ListDefaultViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ItemCell.cellIdentifier) as? ItemCell {
            cell.setupCell(presenter.listItem(at: indexPath))
            cell.delegate = self
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objectId = presenter.listItem(at: indexPath).id
        presenter.listItemSelected(objectId: objectId)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ListDefaultViewController: ItemCellDelegate {
    func addItem(for id: String) {
        presenter.addItem(id: id)
    }

    func removeItem(for id: String) {
        presenter.removeItem(id: id)
    }
}
