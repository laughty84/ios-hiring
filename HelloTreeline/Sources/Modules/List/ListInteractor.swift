//
//  ListInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

enum SendingStatus {
    case success, failure
}

protocol ListInteractorDelegate: AnyObject {
    func itemsDidLoad(items: [InventoryItem])
    func itemsLoadFailed(error: Error)
    func updateSendingStatus(_ status: SendingStatus)
    func updateItem(_ item: InventoryItem)
}

class ListInteractor {

    weak var delegate: ListInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository

    private var items: [InventoryItem] = []

    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.itemRepository = itemRepository
    }
    
    func loadItems() {
        itemRepository.getAll { result in
            switch result {
            case .success(let remoteItems):
                self.items = remoteItems
                self.delegate?.itemsDidLoad(items: remoteItems)
            case .failure(let error):
                self.delegate?.itemsLoadFailed(error: error)
                break
            }
        }
    }

    func sendReport() {
        let csvData = prepareData()
        print(csvData)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            // some awesome sending
            self.delegate?.updateSendingStatus(Bool.random() ? .success : .failure)
        }
    }

    func addToItem(_ id: String) {
        guard let index = items.firstIndex(where: { $0.id == id }) else { return }
            if items[index].requestedAmount < items[index].available {
                items[index].requestedAmount += 1
            }
            delegate?.updateItem(items[index])
    }

    func removeFromItem(_ id: String) {
        guard let index = items.firstIndex(where: { $0.id == id }) else { return }
            items[index].requestedAmount -= 1
            if items[index].requestedAmount < 0 {
                items[index].requestedAmount = 0
            }
            delegate?.updateItem(items[index])
    }

    private func prepareData() -> String {
        return items.description
    }
}
