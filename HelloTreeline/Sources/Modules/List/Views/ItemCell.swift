//
//  ItemCell.swift
//  HelloTreeline
//
//  Created by Piotr Rola on 17/03/2022.
//

import Foundation
import UIKit

protocol ItemCellDelegate: AnyObject {
    func addItem(for id: String)
    func removeItem(for id: String)
}

class ItemCell: UITableViewCell {
    static let cellIdentifier = "ItemCell"

    private enum Constants {
        static let margin: CGFloat = 11
        static let spacing: CGFloat = 4
    }

    private let titleLabel: UILabel = UILabel()
    private let soldAmountLabel: UILabel = UILabel()
    private let addButton: UIButton = UIButton()
    private let removeButton: UIButton = UIButton()

    weak var delegate: ItemCellDelegate?

    private var cellId: String = ""

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupTitle()
        setupSoldAmountLabel()
        setupAddButton()
        setupRemoveButton()
    }

    func setupCell(_ viewModel: ListItem) {
        titleLabel.text = viewModel.title
        soldAmountLabel.text = "\(viewModel.soldAmount)"
        cellId = viewModel.id
    }

    @objc
    func add(){
        delegate?.addItem(for: cellId)
    }

    @objc
    func remove(){
        delegate?.removeItem(for: cellId)
    }

    private func setupTitle() {
        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0

        NSLayoutConstraint.activate([titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.margin),
                                     titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.margin)])

    }

    private func setupSoldAmountLabel() {
        contentView.addSubview(soldAmountLabel)
        soldAmountLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([soldAmountLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.margin),
                                     soldAmountLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constants.spacing),
                                     soldAmountLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.margin)])
    }

    private func setupAddButton() {
        contentView.addSubview(addButton)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.addTarget(self, action: #selector(add), for: .touchUpInside)
        addButton.setTitle("+", for: .normal)
        addButton.backgroundColor = .darkGray
        addButton.setTitleColor(.green, for: .normal)

        NSLayoutConstraint.activate([addButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                                    addButton.leadingAnchor.constraint(greaterThanOrEqualTo: titleLabel.trailingAnchor, constant: Constants.spacing),
                                    addButton.trailingAnchor.constraint(greaterThanOrEqualTo: soldAmountLabel.trailingAnchor, constant: Constants.spacing)])
    }

    private func setupRemoveButton() {
        contentView.addSubview(removeButton)
        removeButton.translatesAutoresizingMaskIntoConstraints = false
        removeButton.addTarget(self, action: #selector(remove), for: .touchUpInside)
        removeButton.setTitle("-", for: .normal)
        removeButton.backgroundColor = .darkGray
        removeButton.setTitleColor(.red, for: .normal)

        NSLayoutConstraint.activate([removeButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                                     removeButton.leadingAnchor.constraint(equalTo:addButton.trailingAnchor, constant: Constants.spacing),
                                    removeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.margin)])
    }

    required init?(coder: NSCoder) { nil }


}
