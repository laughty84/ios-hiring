//
//  ListPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class ListPresenter: ListInteractorDelegate {

    private let router: ListRouter
    private let interactor: ListInteractor
    private weak var view: ListViewController!
    
    private var listItems: [ListItem] = []
    
    var itemCount: Int { listItems.count }

    init(view: ListViewController, interactor: ListInteractor, router: ListRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router

        self.interactor.delegate = self
    }
    
    func viewWillAppear() {
        interactor.loadItems()
    }
    
    func listItem(at indexPath: IndexPath) -> ListItem {
        listItems[indexPath.row]
    }
    
    func listItemSelected(objectId: String) {
        router.routeToDetails(objectId: objectId)
    }
    
    func itemsDidLoad(items: [InventoryItem]) {
        self.listItems = items.map { .init(id: $0.id, title: $0.title) }
        self.view.reloadList()
    }

    func itemsLoadFailed(error: Error) {
        view.displayError(error.localizedDescription)
    }

    func updateItem(_ item: InventoryItem) {
        if let index = listItems.firstIndex(where: { $0.id == item.id }) {
            var listItem: ListItem = .init(id: item.id, title: item.title)
            listItem.soldAmount = item.requestedAmount ?? 0
            listItems[index] = listItem
            view.updateCell(row: index)
        }
    }

    func updateSendingStatus(_ status: SendingStatus) {
        var message: String = ""
        switch status {
        case .success:
            message = "Great Success"
        case .failure:
            message = "Sh** happens"
        }
        view.displaySendStatus(message)
    }

    func sendReport(){
        interactor.sendReport()
    }

    func addItem(id: String) {
        interactor.addToItem(id)
    }

    func removeItem(id: String) {
        interactor.removeFromItem(id)
    }

}
