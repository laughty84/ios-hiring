//
//  InventoryItem.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

struct InventoryItem: Decodable {
    let id: String
    let title: String
    let description: String
    let color: String
    let available: Int
    var cost: Float {
        get { return _cost ?? 0 }
        set { _cost = newValue }
    }
    var requestedAmount: Int {
        get { return _requestedAmount ?? 0 }
        set { _requestedAmount = newValue }
    }

    private var _cost: Float?
    private var _requestedAmount: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case color
        case available
        case _cost = "cost"
        case _requestedAmount = "requestedAmount"
    }

    init(id: String,
         title: String,
         description: String,
         color: String,
         available: Int,
         cost: Float = 0.0,
         requestedAmount: Int = 0){
        self.id = id
        self.title = title
        self.description = description
        self.color = color
        self.available = available
        self.cost = cost
        self.requestedAmount = requestedAmount
    }

}
